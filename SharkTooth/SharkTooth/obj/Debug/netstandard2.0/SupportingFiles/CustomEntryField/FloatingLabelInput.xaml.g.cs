//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Xamarin.Forms.Xaml.XamlResourceIdAttribute("SharkTooth.SupportingFiles.CustomEntryField.FloatingLabelInput.xaml", "SupportingFiles/CustomEntryField/FloatingLabelInput.xaml", typeof(global::SharkTooth.SupportingFiles.CustomEntryField.FloatingLabelInput))]

namespace SharkTooth.SupportingFiles.CustomEntryField {
    
    
    [global::Xamarin.Forms.Xaml.XamlFilePathAttribute("SupportingFiles/CustomEntryField/FloatingLabelInput.xaml")]
    public partial class FloatingLabelInput : global::Xamarin.Forms.ContentView {
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::Xamarin.Forms.ContentView @this;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::Xamarin.Forms.Frame SelectedFrame;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::SharkTooth.CustomRenderer.CustomEntry EntryField;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::Xamarin.Forms.Label LabelTitle;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private void InitializeComponent() {
            global::Xamarin.Forms.Xaml.Extensions.LoadFromXaml(this, typeof(FloatingLabelInput));
            @this = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::Xamarin.Forms.ContentView>(this, "this");
            SelectedFrame = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::Xamarin.Forms.Frame>(this, "SelectedFrame");
            EntryField = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::SharkTooth.CustomRenderer.CustomEntry>(this, "EntryField");
            LabelTitle = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::Xamarin.Forms.Label>(this, "LabelTitle");
        }
    }
}
