﻿using System;

using Xamarin.Forms;

namespace SharkTooth.SupportingFiles
{
    public class Constants
    {
        #region URLs
        public const string APIBaseUrl = "http://site4demo.com/sharktooth/";
        public const string LoginEndpoint = "wp-json/jwt-auth/v1/token";
        public const string SignUpAPI = "api/user/register/?nonce=";
        public const string AllPagesAPI = "wp-json/wp/v2/pages?_embed&per_page=100";
        public const string ForgotPasswordAPI = "api/user/retrieve_password/?nonce=578044792d&insecure=cool";
        public const string HomeAPI = "wp-json/wp/v2/posts?per_page=100";
        public const string SearchAPI = "wp-json/wp/v2/posts?search";
        public const string ShardDetailAPI = "wp-json/wp/v2/posts";
        public const string NonceAPI = "api/get_nonce/?controller=user&method=register";
        public const string EditProfileGetCookie = "api/user/generate_auth_cookie/?controller=user&method=update_user_meta&insecure=cool";
        public const string SaveProfileDetails = "api/user/update_user_meta_vars/?";
        public const string ChangePasswordAPI = "wp-json/wp/v2/users/";
        public const string FeedbackAPI = "wp-json/wp/v2/feedbacks";
        public const string UsersAPI = "wp-json/wp/v2/users";
        public const string GetLoggedInUserIdAPI = "wp-json/userdetail/loggedinuser";

        #endregion

        #region Constant Message
        public static string Somethingwentwrong = "Wrong Username and/or Password. ";
        public static string OK = "OK";
        public const string Error = "Error";
        public const string Success = "Success";

        public static string NameEmpty = "Please enter name";
        public static string PhoneEmpty = "Please enter phone number";
        public static string EmailEmpty = "Please enter email ID";
        public static string PasswordEmpty = "Please enter password";
        public static string ConfirmPasswordEmpty = "Please enter confirm password";
        public static string PasswordDoNotMatch = "Password does not match";
        public const string EmailInvalid = "Invalid email. Please enter valid email address";
        public const string LoginError = "Login error. Please try again!";
        public static string NoDataFound = "No Data Found";
        public const string UnknownError = "Something went wrong. Please try again!";
        public const string SignUpError = "Signup error. Please try again!";
        public const string ChangePasswordSuccess = "Password changed successfully!";
        public const string ChangePasswordFailure = "Password not changed!";
        public const string NewPassordEmpty = "Enter new password";
        public const string OldPasswordWrong = "Current password is not correct.";
        public const string FeedBackSuccess = "Feedback submitted.";
        public const string TermsConditionsError = "Please agree to terms and conditions.";

        #endregion

        #region APIKey
        public static string username = "Email";
        public static string password = "Password";
        public static string rememberMe = "RememberMe";
        public static string deviceType = "DeviceName";
        public static string deviceToken = "DeviceId";

        #endregion
    }
}

