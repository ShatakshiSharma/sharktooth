﻿using System;
using Newtonsoft.Json;
using SharkTooth.Helpers;
using SharkTooth.Models;
using Xamarin.Forms;

namespace SharkTooth.SupportingFiles
{
    public class Global
    {
        public Global()
        {
        }

        private static LoginModel _loggedInUser;
        public static LoginModel LoggedInUser
        {
            get => _loggedInUser;
            set
            {
                _loggedInUser = value;
                if (_loggedInUser != null)
                {
                    SettingsHelper.LoggedInUser = JsonConvert.SerializeObject(_loggedInUser);
                }
            }
        }

        public static string DeviceToken = "";

        public static NavigationPage page1 { get; set; }
        public static NavigationPage page2 { get; set; }
        public static NavigationPage page3 { get; set; }
        public static NavigationPage page4 { get; set; }

    }
}

