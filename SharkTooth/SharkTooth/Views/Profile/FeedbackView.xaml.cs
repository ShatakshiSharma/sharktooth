﻿using System;
using System.Collections.Generic;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.Profile
{
    public partial class FeedbackView : ContentPage
    {
        FeedbackViewModel feedbackViewModel;
        public FeedbackView()
        {
            InitializeComponent();
            feedbackViewModel = new FeedbackViewModel(Navigation);
            BindingContext = feedbackViewModel;
        }
    }
}
