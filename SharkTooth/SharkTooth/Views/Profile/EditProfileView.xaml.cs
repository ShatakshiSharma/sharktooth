﻿using System;
using System.Collections.Generic;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.Profile
{
    public partial class EditProfileView : ContentPage
    {
        EditProfileViewModel editProfileViewModel;
        public EditProfileView()
        {
            InitializeComponent();
            editProfileViewModel = new EditProfileViewModel(Navigation);
            BindingContext = editProfileViewModel;
        }
    }
}
