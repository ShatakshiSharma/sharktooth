﻿using System;
using System.Collections.Generic;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.Profile
{
    public partial class AboutUsView : ContentPage
    {
        AboutUsViewModel aboutUsViewModel;
        public AboutUsView()
        {
            InitializeComponent();
            aboutUsViewModel = new AboutUsViewModel(Navigation);
            BindingContext = aboutUsViewModel;

        }
    }
}
