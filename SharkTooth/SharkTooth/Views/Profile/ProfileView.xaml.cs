﻿using System;
using System.Collections.Generic;
using SharkTooth.Helpers;
using SharkTooth.ViewModels;
using SharkTooth.Views.LoginSignUp;
using Xamarin.Forms;

namespace SharkTooth.Views.Profile
{
    public partial class ProfileView : ContentPage
    {
        ProfileViewModel profileViewModel;

        [Obsolete]
        public ProfileView()
        {
            InitializeComponent();
            profileViewModel = new ProfileViewModel(Navigation);
            BindingContext = profileViewModel;

            MessagingCenter.Subscribe<App, string>(App.Current, "Logout", (snd, arg) =>
            {
                logout();
            });
        }

        [Obsolete]
        private void logout()
        {
            Device.BeginInvokeOnMainThread(new Action(async () =>
            {
                if (await DisplayAlert("", "Are you sure you want to log out?", "Yes", "No"))
                {

                    DependencyService.Get<SettingsHelper>().ClearAll();
                    await Navigation.PopToRootAsync(false);
                    await Navigation.PopToRootAsync(false);
                    //await Navigation.PushAsync(new PreferenceView());
                    App.Current.MainPage = new NavigationPage(new SignUpPage());
                }
            }));
        }
    }
}
