﻿using System;
using System.Collections.Generic;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.ForgotPassword
{
    public partial class ForgotPasswordView : ContentPage
    {
        ForgotPasswordViewModel forgotPasswordViewModel;
        public ForgotPasswordView()
        {
            InitializeComponent();
            forgotPasswordViewModel = new ForgotPasswordViewModel(Navigation);
            BindingContext = forgotPasswordViewModel;
        }
    }
}
