﻿using System;
using System.Collections.Generic;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.ForgotPassword
{
    public partial class EmailConfimationView : ContentPage
    {
        EmailConfirmationViewModel emailConfimationViewModel;
        public EmailConfimationView()
        {
            InitializeComponent();
            emailConfimationViewModel = new EmailConfirmationViewModel(Navigation);
            BindingContext = emailConfimationViewModel;
        }
    }
}
