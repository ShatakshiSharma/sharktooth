﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharkTooth.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SharkTooth.Views.FoodAndBeverage
{
    public partial class FoodBeverageView : ContentPage
    {
        FoodBeverageViewModel foodBeverageViewModel;
        public FoodBeverageView()
        {
            InitializeComponent();
            foodBeverageViewModel = new FoodBeverageViewModel(Navigation);
            BindingContext = foodBeverageViewModel;
            GetLocation();
        }

        public async void GetLocation()
        {
            try
            {
                var cameraPermision = await Permissions.RequestAsync<Permissions.Camera>();
                var location = await Geolocation.GetLastKnownLocationAsync();
                
                if (location == null)
                {
                    location = await Geolocation.GetLocationAsync(new GeolocationRequest
                    {
                        DesiredAccuracy = GeolocationAccuracy.Medium,
                        Timeout = TimeSpan.FromSeconds(30)
                    });
                }
                else
                {
                    var placeMark = await Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);
                    var PLaceMarks = placeMark?.FirstOrDefault();
                    if (PLaceMarks != null)
                    {
                        string[] locationVal = { PLaceMarks.CountryName , PLaceMarks.FeatureName + ", " +
                            PLaceMarks.SubLocality + ", " +
                            PLaceMarks.SubAdminArea + ", " +
                            PLaceMarks.AdminArea};
                        MessagingCenter.Send<App, string>(App.Current as App, "City", PLaceMarks.Locality);
                        MessagingCenter.Send<App, string>(App.Current as App, "Latitude", PLaceMarks.Location.Latitude.ToString());
                        MessagingCenter.Send<App, string>(App.Current as App, "Longitude", PLaceMarks.Location.Longitude.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something is wrong: {ex.Message}");
            }
        }
    }
}
