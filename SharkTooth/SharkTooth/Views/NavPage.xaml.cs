﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharkTooth.SupportingFiles;
using SharkTooth.Views.FoodAndBeverage;
using SharkTooth.Views.Home;
using SharkTooth.Views.Profile;
using SharkTooth.Views.Scan;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

namespace SharkTooth.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class NavPage : Xamarin.Forms.TabbedPage
    {
        public NavPage()
        {
            InitializeComponent();
            //CurrentPageChanged += CurrentPageHasChanged;
            this.On<Xamarin.Forms.PlatformConfiguration.Android>().SetIsSwipePagingEnabled(false);
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetBackButtonTitle(this, "");
            
            Global.page1 = new NavigationPage(new HomePage()) { Title = "Home", IconImageSource = "home.png", BarTextColor = Color.Black };
            Global.page2 = new NavigationPage(new ScanPage()) { Title = "Scan", IconImageSource = "scanner.png" };
            Global.page3 = new NavigationPage(new FoodBeverageView()) { Title = "F&B", IconImageSource = "food.png" };
            Global.page4 = new NavigationPage(new ProfileView()) { Title = "Profile", IconImageSource = "profile.png" };

            this.Children.Add(Global.page1);
            this.Children.Add(Global.page2);
            this.Children.Add(Global.page3);
            this.Children.Add(Global.page4);

            SelectedItem = Children[0];
        }

        private void CurrentPageHasChanged(object sender, EventArgs e)
        {
            var tabbedPage = (Xamarin.Forms.TabbedPage)sender;
            //Title = tabbedPage.CurrentPage.Title;
            MessagingCenter.Send<App, string>(App.Current as App, "TabTitle", tabbedPage.CurrentPage.Title);
        }
    }
}
