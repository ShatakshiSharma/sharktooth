﻿using System;
using System.Collections.Generic;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.TermsAndConditions
{
    public partial class TermsAndConditionsView : ContentPage
    {
        TermsAndConditionsViewModel termsAndConditionsViewModel;
        public TermsAndConditionsView()
        {
            InitializeComponent();
            termsAndConditionsViewModel = new TermsAndConditionsViewModel(Navigation);
            BindingContext = termsAndConditionsViewModel;
        }
    }
}
