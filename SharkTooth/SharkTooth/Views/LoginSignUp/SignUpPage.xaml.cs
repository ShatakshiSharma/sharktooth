﻿using System;
using System.Collections.Generic;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.LoginSignUp
{
    public partial class SignUpPage : ContentPage
    {
        SignUpViewModel viewModel { get; set; }

        [Obsolete]
        public SignUpPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new SignUpViewModel(Navigation);

        }
    }
}
