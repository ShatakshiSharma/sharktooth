﻿using System;
using System.Collections.Generic;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.LoginSignUp
{
    public partial class LoginPage : ContentPage
    {
        LoginViewModel viewModel { get; set; }

        public LoginPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new LoginViewModel();

        }
    }
}
