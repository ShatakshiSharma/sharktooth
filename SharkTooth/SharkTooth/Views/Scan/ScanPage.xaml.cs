﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SharkTooth.SupportingFiles;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace SharkTooth.Views.Scan
{
    public partial class ScanPage : ContentPage
    {
        public ScanPage()
        {
            InitializeComponent();
            

            MessagingCenter.Subscribe<App, string>(App.Current, "TabTitle", (snd, arg) =>
            {

                if (arg.Equals("Scan"))
                {
                    
                    ZxingScanner.IsAnalyzing = true;
                    ZxingScanner.IsScanning = true;
                }
                
            });
        }

        public void ZXingScannerView_OnScanResult(ZXing.Result result)
        {
            Device.BeginInvokeOnMainThread(new Action(async () =>
            {
                //ZxingScanner.IsScanning = false;
                //Console.WriteLine(result.Text + "(type: " + result.BarcodeFormat.ToString() + ")");
                await Application.Current.MainPage.DisplayAlert("Scanned Result", result.Text, Constants.OK);
                //ZxingScanner.IsScanning = true;
                
            }));
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            
        }

        



    }
}
