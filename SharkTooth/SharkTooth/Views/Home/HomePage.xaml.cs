﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharkTooth.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SharkTooth.Views.Home
{
    public partial class HomePage : ContentPage
    {
        HomeViewModel viewModel { get; set; }

        [Obsolete]
        public HomePage()
        {
            InitializeComponent();
            BindingContext = viewModel = new HomeViewModel(Navigation);
            //GetLocation();
        }

        //protected override async void OnAppearing()
        //{
        //    base.OnAppearing();
        //    await viewModel.CallHomeDataAsync();
        //    HomeListView.ItemsSource = null;
        //    HomeListView.ItemsSource = viewModel.PopularList;

        //}

        void HomeListView_ItemTapped(System.Object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            viewModel.GoToHomeDetailPage((Models.HomeModel)e.Item);
        }

        public async void GetLocation()
        {
            try
            {
                var location = await Geolocation.GetLastKnownLocationAsync();
                if (location == null)
                {
                    location = await Geolocation.GetLocationAsync(new GeolocationRequest
                    {
                        DesiredAccuracy = GeolocationAccuracy.Medium,
                        Timeout = TimeSpan.FromSeconds(30)
                    });
                }
                else
                {
                    var placeMark = await Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);
                    var PLaceMarks = placeMark?.FirstOrDefault();
                    if (PLaceMarks != null)
                    {
                        string[] locationVal = { PLaceMarks.CountryName , PLaceMarks.FeatureName + ", " +
                            PLaceMarks.SubLocality + ", " +
                            PLaceMarks.SubAdminArea + ", " +
                            PLaceMarks.AdminArea};
                        MessagingCenter.Send<App, string>(App.Current as App, "City_Home", PLaceMarks.Locality);
                        //MessagingCenter.Send<App, string>(App.Current as App, "Latitude", PLaceMarks.Location.Latitude.ToString());
                        //MessagingCenter.Send<App, string>(App.Current as App, "Longitude", PLaceMarks.Location.Longitude.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something is wrong: {ex.Message}");
            }
        }

    }
}

