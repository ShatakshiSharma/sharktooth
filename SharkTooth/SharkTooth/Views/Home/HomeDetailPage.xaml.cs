﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SharkTooth.Models;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.Home
{
    public partial class HomeDetailPage : ContentPage
    {
        HomeDetailViewModel viewModel { get; set; }

        public HomeDetailPage(HomeModel homeModel)
        {
            InitializeComponent();
            BindingContext = viewModel = new HomeDetailViewModel(homeModel);

        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}
