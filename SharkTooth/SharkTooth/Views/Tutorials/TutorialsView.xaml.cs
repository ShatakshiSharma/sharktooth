﻿using System;
using System.Collections.Generic;
using SharkTooth.Models;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.Tutorials
{
    public partial class TutorialsView : ContentPage
    {
        TutorialsViewModel tutorialsViewModel;
        public TutorialsView()
        {
            InitializeComponent();
            tutorialsViewModel = new TutorialsViewModel(Navigation);
            BindingContext = tutorialsViewModel;
        }

        public void OnCardChanged(object sender, CurrentItemChangedEventArgs e)
        {
            //if(((SavedCardItem)e.CurrentItem).Heading.Equals("Search for Food outlets"))
            //{
            //    nextOrContinueButton.Text = "Continue";
            //}
            //else
            //{
            //    nextOrContinueButton.Text = "Next";
            //}
        }

        public void onNextClicked(object sender, EventArgs args)
        {
            if (carouselVi.Position == 0)
            {
                carouselVi.ScrollTo(1);
            }
            else if (carouselVi.Position == 1)
            {
                carouselVi.ScrollTo(2);
            }
        }
    }
}
