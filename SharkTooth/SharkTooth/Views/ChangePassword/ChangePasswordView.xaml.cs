﻿using System;
using System.Collections.Generic;
using SharkTooth.ViewModels;
using Xamarin.Forms;

namespace SharkTooth.Views.ChangePassword
{
    public partial class ChangePasswordView : ContentPage
    {
        ChangePasswordViewModel changePasswordViewModel;
        public ChangePasswordView()
        {
            InitializeComponent();
            changePasswordViewModel = new ChangePasswordViewModel(Navigation);
            BindingContext = changePasswordViewModel;
        }
    }
}
