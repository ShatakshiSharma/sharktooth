﻿using System;
using SharkTooth.Helpers;
using SharkTooth.SupportingFiles;
using SharkTooth.Views;
using SharkTooth.Views.FoodAndBeverage;
using SharkTooth.Views.ForgotPassword;
using SharkTooth.Views.LoginSignUp;
using SharkTooth.Views.Profile;
using SharkTooth.Views.Tutorials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SharkTooth
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            RegisterDependencyServices();
            DependencyService.Get<SettingsHelper>().ReadSettings();

            if (SettingsHelper.IsUserLoggedIn)
            {
                MainPage = new NavPage();
            }
            else
            {
                MainPage = new NavigationPage(new SignUpPage());
            }

        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        private void RegisterDependencyServices()
        {
            DependencyService.Register<SettingsHelper>();
        }
    }
}
