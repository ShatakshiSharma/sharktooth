﻿using System;
namespace SharkTooth.Interfaces
{
    public interface ILoaderInterface
    {
        void Show(string title = "");
        void Hide();
    }
}
