﻿using System;
using Newtonsoft.Json;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SharkTooth.Helpers
{
    public class SettingsHelper
    {
        public static bool IsUserLoggedIn
        {
            get => Preferences.Get(nameof(IsUserLoggedIn), false);
            set => Preferences.Set(nameof(IsUserLoggedIn), value);
        }

        public static string LoggedInUser
        {
            get => Preferences.Get(nameof(LoggedInUser), "");
            set => Preferences.Set(nameof(LoggedInUser), value);
        }

        public static string SavedToken
        {
            get => Preferences.Get(nameof(SavedToken), "");
            set => Preferences.Set(nameof(SavedToken), value);
        }

        public string CancelOrSelect
        {
            get => Preferences.Get(nameof(CancelOrSelect), "");
            set => Preferences.Set(nameof(CancelOrSelect), value);
        }

        public bool loadSearchCandidateDataOrNot
        {
            get => Preferences.Get(nameof(loadSearchCandidateDataOrNot), true);
            set => Preferences.Set(nameof(loadSearchCandidateDataOrNot), value);
        }

        public SettingsHelper()
        {

        }

        public void ReadSettings()
        {
            try
            {
                if (IsUserLoggedIn)
                {
                    Global.LoggedInUser = JsonConvert.DeserializeObject<LoginModel>(LoggedInUser);
                }
            }
            catch (Exception exc)
            {
                //DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
            }
        }

        public void ClearAll()
        {
            Preferences.Clear();
        }

        public void Logout()
        {
            Preferences.Remove(nameof(IsUserLoggedIn));
            Preferences.Remove(nameof(LoggedInUser));
            Preferences.Remove(nameof(SavedToken));
        }

    }
}

