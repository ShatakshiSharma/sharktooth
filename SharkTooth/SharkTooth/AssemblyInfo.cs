using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("Oswald-Bold.ttf")]
[assembly: ExportFont("Oswald-ExtraLight.ttf")]
[assembly: ExportFont("Oswald-Light.ttf")]
[assembly: ExportFont("Oswald-Medium.ttf")]
[assembly: ExportFont("Oswald-Regular.ttf")]
[assembly: ExportFont("Oswald-SemiBold.ttf")]