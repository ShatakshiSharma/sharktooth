﻿using System;
using System.Threading.Tasks;

namespace SharkTooth.Service
{
    public interface IQrScanningService
    {
        Task<string> ScanAsync();
    }
}
