﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using SharkTooth.Views;
using SharkTooth.Views.LoginSignUp;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class SignUpViewModel : BaseViewModel
    {
        INavigation navigation;
        public ICommand SignInCommand { get; set; }
        public ICommand SubmitCommand { get; set; }

        private SignUpDataAccess _signUpDataAccess = new SignUpDataAccess();

        private string nonce = "";
        public string Nonce
        {
            get
            {
                return nonce;
            }
            set
            {
                SetProperty(ref nonce, value);
            }
        }

        private string name = "";
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                SetProperty(ref name, value);
            }
        }

        private string phone = "";
        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                SetProperty(ref phone, value);
            }
        }

        private bool isChecked = false;
        public bool Is_Checked
        {
            get
            {
                return isChecked;
            }
            set
            {
                SetProperty(ref isChecked, value);
            }
        }

        private string email = "";
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetProperty(ref email, value);
            }
        }

        private string password = "";
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                SetProperty(ref password, value);
            }
        }

        private string confirmPassword = "";
        public string ConfirmPassword
        {
            get
            {
                return confirmPassword;
            }
            set
            {
                SetProperty(ref confirmPassword, value);
            }
        }

        [Obsolete]
        public SignUpViewModel(INavigation nav)
        {
            navigation = nav;
            SignInCommand = new Command(SignInAction);
            SubmitCommand = new Command(SubmitAction);
        }

        [Obsolete]
        private async void SubmitAction()
        {
            await GetNonce();
        }

        [Obsolete]
        private async void SignInAction()
        {
            LoginPage obj = new LoginPage();
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }

        [Obsolete]
        public async Task GetNonce()
        {
            DependencyService.Get<ILoaderInterface>().Show("Wait..");
            RegisterNonce nonceModel = await _signUpDataAccess.CallGetNonce();
            if (nonceModel != null)
            {
                Nonce = nonceModel.nonce;
            }
            DependencyService.Get<ILoaderInterface>().Hide();
            await SignUpAsync();
        }

        [Obsolete]
        public async Task SignUpAsync()
        {
            if (!Is_Checked)
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.TermsConditionsError, Constants.OK);
                return;
            }

            if (await ValidateFieldsAsync())
            {
                try
                {
                    DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                    bool isSuccess = await _signUpDataAccess.CallPostSignUpAPI(Email, Password, Email, Phone, Name, Nonce);
                    DependencyService.Get<ILoaderInterface>().Hide();
                    if (isSuccess)
                    {
                        Email = "";
                        Password = "";
                        Name = "";
                        Phone = "";
                        ConfirmPassword = "";

                        LoginPage obj = new LoginPage();
                        await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(obj);
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.UnknownError, Constants.OK);
                    }
                }
                catch (Exception exc)
                {
                    // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
                }
            }
        }

        private async Task<bool> ValidateFieldsAsync()
        {
            bool success = true;

            if (string.IsNullOrEmpty(Name))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.NameEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(Phone))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.PhoneEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(Password))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.PasswordEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(ConfirmPassword))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.ConfirmPasswordEmpty, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailInvalid, Constants.OK);
                success = false;
            }
            else if (Password != ConfirmPassword)
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.PasswordDoNotMatch, Constants.OK);
                success = false;
            }

            return success;
        }
    }
}

