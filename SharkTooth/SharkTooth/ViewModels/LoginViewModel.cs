﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using SharkTooth.Views;
using SharkTooth.Views.ForgotPassword;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        INavigation navigation;
        public ICommand LoginCommand { get; set; }
        public ICommand ForgotPasswordCommand { get; set; }
        public ICommand SignInCommand { get; set; }
        private LoginDataAccess _loginDataAccess = new LoginDataAccess();

        private string email = "";
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetProperty(ref email, value);
            }
        }

        private string password = "";
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                SetProperty(ref password, value);
            }
        }

        public LoginViewModel()
        {
            LoginCommand = new Command(LoginAction);
            ForgotPasswordCommand = new Command(ForgotPasswordAction);
            SignInCommand = new Command(RegisterAction);
        }

        public async void ForgotPasswordAction()
        {
            ForgotPasswordView obj = new ForgotPasswordView();
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }

        [Obsolete]
        private async void LoginAction()
        {
            await LoginAsync();
        }

        private async void RegisterAction()
        {
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PopModalAsync();
        }


        [Obsolete]
        public async Task LoginAsync()
        {
            if (await ValidateFieldsAsync())
            {
                try
                {
                    DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                    bool isSuccess = await _loginDataAccess.CallPostLoginAPI(Email, Password);
                    DependencyService.Get<ILoaderInterface>().Hide();
                    if (isSuccess)
                    {

                        string getId = await _loginDataAccess.CallLoggedInUserIdAPI();
                        if(getId != "")
                        {
                            Global.LoggedInUser.id = getId;
                            Global.LoggedInUser = Global.LoggedInUser;
                            UserModel userModel = await _loginDataAccess.GetUserData();
                            if (userModel != null)
                            {
                                if (!userModel.phone_number.Equals(""))
                                {
                                    Global.LoggedInUser.phone_number = userModel.phone_number;
                                    Global.LoggedInUser = Global.LoggedInUser;
                                }
                            }
                        }
                        Helpers.SettingsHelper.IsUserLoggedIn = true;
                        Email = "";
                        Password = "";

                        Application.Current.MainPage = new NavPage();

                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.LoginError, Constants.OK);
                    }
                }
                catch (Exception exc)
                {
                    // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
                }
            }
        }

        private async Task<bool> ValidateFieldsAsync()
        {
            bool success = true;

            if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(Password))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.PasswordEmpty, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailInvalid, Constants.OK);
                success = false;
            }

            return success;
        }
    }
}

