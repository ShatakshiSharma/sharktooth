﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class ChangePasswordViewModel : BaseViewModel
    {
        INavigation navigation;
        public ICommand BackCommand { get; set; }
        public ICommand DissmissCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        private string oldPassword = "";
        public string OldPassword
        {
            get
            {
                return oldPassword;
            }
            set
            {
                SetProperty(ref oldPassword, value);
            }
        }

        private string newPassword = "";
        public string NewPassword
        {
            get
            {
                return newPassword;
            }
            set
            {
                SetProperty(ref newPassword, value);
            }
        }

        private string confirmPassword = "";
        public string ConfirmPassword
        {
            get
            {
                return confirmPassword;
            }
            set
            {
                SetProperty(ref confirmPassword, value);
            }
        }

        public ChangePasswordViewModel(INavigation nav)
        {
            navigation = nav;
            BackCommand = new Command(BackAction);
            DissmissCommand = new Command(DissmissAction);
            SaveCommand = new Command(ChangePasswordAPI);
        }

        public void BackAction()
        {
            navigation.PopModalAsync();
        }

        public void DissmissAction()
        {
            navigation.PopModalAsync();
        }

        

        public async void ChangePasswordAPI()
        {
            if(await ValidateFieldsAsync())
            {
                try
                {
                    DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                    ChangePasswordModel changePasswordModel = await ProfileDataAccess.CallChangePasswordAPI(NewPassword);
                    DependencyService.Get<ILoaderInterface>().Hide();

                    if (changePasswordModel.username != null || !changePasswordModel.username.Equals(""))
                    {
                        await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.ChangePasswordSuccess, Constants.OK);
                        await navigation.PopModalAsync();
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.ChangePasswordFailure, Constants.OK);
                        await navigation.PopModalAsync();
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            
        }

        private async Task<bool> ValidateFieldsAsync()
        {
            bool success = true;

            if (string.IsNullOrEmpty(OldPassword))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.PasswordEmpty, Constants.OK);
                success = false;
            }
            else if (!OldPassword.Equals(Global.LoggedInUser.user_password))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.OldPasswordWrong, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(NewPassword))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.NewPassordEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(ConfirmPassword))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.ConfirmPasswordEmpty, Constants.OK);
                success = false;
            }
            else if (!NewPassword.Equals(ConfirmPassword))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.PasswordDoNotMatch, Constants.OK);
                success = false;
            }

            return success;
        }
    }
}
