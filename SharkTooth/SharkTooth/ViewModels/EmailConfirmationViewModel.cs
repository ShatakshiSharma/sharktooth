﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class EmailConfirmationViewModel : INotifyPropertyChanged
    {
        INavigation navigation;
        public ICommand BackCommand { get; set; }
        public ICommand DissmissCommand { get; set; }
        public EmailConfirmationViewModel(INavigation nav)
        {
            navigation = nav;
            BackCommand = new Command(BackAction);
            DissmissCommand = new Command(DissmissAction);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetProperty<T>(ref T storage, T value,
                                     [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        public void BackAction()
        {
            navigation.PopModalAsync();
        }

        public void DissmissAction()
        {
            navigation.PopModalAsync();
        }



        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        }
    }
}
