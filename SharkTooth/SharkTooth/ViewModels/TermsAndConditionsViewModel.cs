﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class TermsAndConditionsViewModel : BaseViewModel
    {
        INavigation navigation;
        public ICommand BackCommand { get; set; }
        public TermsAndConditionsViewModel(INavigation nav)
        {
            navigation = nav;
            BackCommand = new Command(BackAction);
            GetTermsAndConditions();
        }

        

        public async void BackAction()
        {
            await navigation.PopAsync();
        }

        private string termsConditionContent = "";
        public string TermsConditionContent
        {
            get
            {
                return termsConditionContent;
            }
            set
            {
                SetProperty(ref termsConditionContent, value);
            }
        }

        public async void GetTermsAndConditions()
        {
            try
            {
                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                ObservableCollection<Root> allPagesAPIModel = await TutorialsDataAccess.CallAllPages();
                DependencyService.Get<ILoaderInterface>().Hide();

                if (allPagesAPIModel != null)
                {
                    for (int i = 0; i < allPagesAPIModel.Count; i++)
                    {
                        if (allPagesAPIModel[i].id == 141)
                        {
                            TermsConditionContent = allPagesAPIModel[i].content.rendered;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
