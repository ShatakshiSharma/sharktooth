﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using SharkTooth.SupportingFiles;
using SharkTooth.Views.ChangePassword;
using SharkTooth.Views.Profile;
using SharkTooth.Views.TermsAndConditions;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class ProfileViewModel : INotifyPropertyChanged
    {
        INavigation navigation;
        public ICommand EditProfileCommand { get; set; }
        public ICommand ChangePasswordCommand { get; set; }
        public ICommand FeedbackCommand { get; set; }
        public ICommand AboutUSCommand { get; set; }
        public ICommand ShareAppCommand { get; set; }
        public ICommand LogoutCommand { get; set; }
        public ICommand TermsConditionsCommand { get; set; }
        public ProfileViewModel(INavigation nav)
        {
            navigation = nav;
            Username = Global.LoggedInUser.user_display_name;
            Email = Global.LoggedInUser.user_email;
            EditProfileCommand = new Command(EditProfileAction);
            ChangePasswordCommand = new Command(ChangePasswordAction);
            AboutUSCommand = new Command(AboutUsAction);
            ShareAppCommand = new Command(ShareAppAction);
            FeedbackCommand = new Command(FeedbackAction);
            LogoutCommand = new Command(LogoutAction);
            TermsConditionsCommand = new Command(TermsAndConditionsAction);

            MessagingCenter.Subscribe<App, string>(App.Current, "UserNameUpdate", (snd, arg) =>
            {
                Username = arg;
            });
        }

        private async void LogoutAction()
        {
            MessagingCenter.Send<App, string>(App.Current as App, "Logout", "");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetProperty<T>(ref T storage, T value,
                                     [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _email = "";
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                SetProperty(ref _email, value);
            }
        }

        public async void FeedbackAction()
        {
            await navigation.PushAsync(new FeedbackView());
        }

        public async void ShareAppAction()
        {
            await Share.RequestAsync(new ShareTextRequest
            {
                Text = "Sample text",
                Title = "Heading"
            });
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        }

        public async void AboutUsAction()
        {
            await navigation.PushAsync(new AboutUsView());
        }

        public async void EditProfileAction()
        {
            await navigation.PushAsync(new EditProfileView());
        }

        public async void ChangePasswordAction()
        {
            await navigation.PushModalAsync(new ChangePasswordView());
        }

        public async void TermsAndConditionsAction()
        {
            await navigation.PushAsync(new TermsAndConditionsView());
        }
    }
}
