﻿using System;
using System.Collections.ObjectModel;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class HomeDetailViewModel : BaseViewModel
    {
        private HomeDataAccess _homeDataAccess = new HomeDataAccess();
        private ObservableCollection<FileImageSource> imageSources = new ObservableCollection<FileImageSource>();
        public ObservableCollection<FileImageSource> ImageSources
        {
            get => this.imageSources;
            set
            {
                this.imageSources = value;
                OnPropertyChanged(nameof(ImageSources));
            }
        }

        private string sharmName = "";
        public string SharkName
        {
            get
            {
                return sharmName;
            }
            set
            {
                SetProperty(ref sharmName, value);
            }
        }

        private string lifeSpan = "";
        public string LifeSpan
        {
            get
            {
                return lifeSpan;
            }
            set
            {
                SetProperty(ref lifeSpan, value);
            }
        }

        private string length = "";
        public string Length
        {
            get
            {
                return length;
            }
            set
            {
                SetProperty(ref length, value);
            }
        }

        private string mass = "";
        public string Mass
        {
            get
            {
                return mass;
            }
            set
            {
                SetProperty(ref mass, value);
            }
        }

        private string description = "";
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                SetProperty(ref description, value);
            }
        }

        public HomeDetailViewModel(HomeModel homeModel)
        {
            ImageSources.Add("LoginBackgroundImage.png");
            ImageSources.Add("Nearby_icon.png");
            ImageSources.Add("TutInfoImage.png");

            OnPropertyChanged(nameof(ImageSources));
            GetSharkDetail(homeModel.id.ToString());

        }

        public async void GetSharkDetail(string id)
        {
            try
            {
                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                HomeModel homeModel = await _homeDataAccess.CallSharkDetailsAPI(id);
                DependencyService.Get<ILoaderInterface>().Hide();

                if(homeModel != null)
                {
                    SharkName = homeModel.title.rendered;
                    LifeSpan = homeModel.acf_fields.life_span;
                    Length = homeModel.acf_fields.length;
                    Mass = homeModel.acf_fields.mass;
                    Description = homeModel.content.rendered;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }


        }
    }
}

