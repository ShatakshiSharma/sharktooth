﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using SharkTooth.Views.ForgotPassword;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class ForgotPasswordViewModel : INotifyPropertyChanged
    {
        INavigation navigation;
        public ICommand SendCommand { get; set; }
        public ICommand BackCommand { get; set; }
        private string email = "";
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetProperty(ref email, value);
            }
        }
        public ForgotPasswordViewModel(INavigation nav)
        {
            navigation = nav;
            SendCommand = new Command(SendAction);
            BackCommand = new Command(BackAction);
        }

        public void BackAction()
        {
            navigation.PopModalAsync();
        }

        public async void SendAction()
        {
            if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailEmpty, Constants.OK);
                return;
            }

            DependencyService.Get<ILoaderInterface>().Show("Wait..");
            ForgotPasswordModel forgotPasswordModel = await SignUpDataAccess.CallForgotPasswordAPI(Email);
            DependencyService.Get<ILoaderInterface>().Hide();
            if (forgotPasswordModel != null)
            {
                if (forgotPasswordModel.status.Equals("ok"))
                {
                    await navigation.PushModalAsync(new EmailConfimationView());
                }
            }
            }
            

        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetProperty<T>(ref T storage, T value,
                                     [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }



        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        }
    }
}
