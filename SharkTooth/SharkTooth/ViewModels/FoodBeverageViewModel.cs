﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SharkTooth.SupportingFiles;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class FoodBeverageViewModel : INotifyPropertyChanged
    {
        INavigation navigation;

        private string username = "";
        public string UserName
        {
            get
            {
                return username;
            }
            set
            {
                SetProperty(ref username, value);
            }
        }

        private string location = "";
        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                SetProperty(ref location, value);
            }
        }

        public FoodBeverageViewModel(INavigation nav)
        {
            navigation = nav;
            UserName = "Hello " + Global.LoggedInUser.user_display_name.ToUpper() + ",";
            MessagingCenter.Subscribe<App, string>(App.Current, "City", (snd, arg) =>
            {
                Location = arg;
            });

            MessagingCenter.Subscribe<App, string>(App.Current, "AddCountry", (snd, arg) =>
            {
                //Country = arg;
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;


        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        }

        protected bool SetProperty<T>(ref T storage, T value,
                                     [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}
