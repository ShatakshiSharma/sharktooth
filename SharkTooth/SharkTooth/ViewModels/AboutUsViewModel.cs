﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class AboutUsViewModel : BaseViewModel
    {
        INavigation navigation;
        public ICommand BackCommand { get; set; }
        public AboutUsViewModel(INavigation nav)
        {
            navigation = nav;
            BackCommand = new Command(BackAction);
            GetAboutUS();
        }

        private string aboutUsContent = "";
        public string AboutUsContent
        {
            get
            {
                return aboutUsContent;
            }
            set
            {
                SetProperty(ref aboutUsContent, value);
            }
        }

        public async void BackAction()
        {
            await navigation.PopAsync();
        }

        public async void GetAboutUS()
        {
            try
            {
                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                ObservableCollection<Root> allPagesAPIModel = await TutorialsDataAccess.CallAllPages();
                DependencyService.Get<ILoaderInterface>().Hide();

                if(allPagesAPIModel != null)
                {
                    for(int i = 0; i < allPagesAPIModel.Count; i++)
                    {
                        if(allPagesAPIModel[i].id == 10)
                        {
                            AboutUsContent = allPagesAPIModel[i].content.rendered;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
