﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class EditProfileViewModel : INotifyPropertyChanged
    {
        INavigation Navigation;
        public ICommand BackCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public EditProfileViewModel(INavigation nav)
        {
            Navigation = nav;
            BackCommand = new Command(BackAction);
            SaveCommand = new Command(GetCookieAPI);
            Email = Global.LoggedInUser.user_email;
            Name = Global.LoggedInUser.user_display_name;
            PhoneNumber = Global.LoggedInUser.phone_number;
        }

        private string name = "";
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                SetProperty(ref name, value);
            }
        }

        private string phoneNumber = "";
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                SetProperty(ref phoneNumber, value);
            }
        }

        private string email = "";
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetProperty(ref email, value);
            }
        }

        public void BackAction()
        {
            Navigation.PopAsync();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetProperty<T>(ref T storage, T value,
                                     [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        }

        public async void GetCookieAPI()
        {
            DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
            CookieModel cookieModel = await ProfileDataAccess.CallCookieAPI(Global.LoggedInUser.user_email, Global.LoggedInUser.user_password);
            DependencyService.Get<ILoaderInterface>().Hide();

            if(cookieModel != null)
            {
                if(cookieModel.cookie != null)
                { 
                    SaveProfileDetails(cookieModel.cookie);
                }
            }
        }

        public async void SaveProfileDetails(string cookie)
        {
            DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
            EditProfileModel editProfileModel = await ProfileDataAccess.SaveProfileDataAPi(cookie, Name, PhoneNumber);
            DependencyService.Get<ILoaderInterface>().Hide();

            if(editProfileModel.status.Equals("ok"))
            {
                Global.LoggedInUser.user_display_name = Name;
                Global.LoggedInUser = Global.LoggedInUser;
                MessagingCenter.Send<App, string>(App.Current as App, "UserNameUpdate", Name);
                //await Navigation.PopModalAsync();
            }
        }
    }
}
