﻿using System;
using System.Windows.Input;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class FeedbackViewModel : BaseViewModel
    {
        INavigation navigation;
        public ICommand SubmitCommand { get; set; }
        public ICommand BackCommand { get; set; }
        private string feedback = "";
        public string Feedback
        {
            get
            {
                return feedback;
            }
            set
            {
                SetProperty(ref feedback, value);
            }
        }
        public FeedbackViewModel(INavigation nav)
        {
            navigation = nav;
            SubmitCommand = new Command(SubmitAction);
            BackCommand = new Command(BackAction);
        }

        public async void SubmitAction()
        {
            DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
            FeedbackModel feedbackModel = await ProfileDataAccess.CallFeedbackAPI(Feedback);
            DependencyService.Get<ILoaderInterface>().Hide();

            if(feedbackModel != null)
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.FeedBackSuccess, Constants.OK);
                await navigation.PopAsync();
            }
        }

        public async void BackAction()
        {
           await navigation.PopAsync();
        }
    }
}
