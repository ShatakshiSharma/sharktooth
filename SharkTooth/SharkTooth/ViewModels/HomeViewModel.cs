﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Input;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using SharkTooth.Views.Home;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        INavigation navigation;
        private HomeDataAccess _homeDataAccess = new HomeDataAccess();
        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string searchText = "";
        public string SS
        {
            get
            {
                return searchText;
            }
            set
            {
                SetProperty(ref searchText, value);
            }
        }


        private string address = "";
        public string Address
        {
            get { return address; }
            set { SetProperty(ref address, value); }
        }

        private ObservableCollection<HomeModel> _popularList = new ObservableCollection<HomeModel>();
        public ObservableCollection<HomeModel> PopularList
        {
            get => _popularList;
            set
            {
                _popularList = value;
                OnPropertyChanged(nameof(PopularList));
            }
        }

        private bool _labelIsVisible = false;
        public bool LabelIsVisible
        {
            get { return _labelIsVisible; }
            set
            {
                _labelIsVisible = value;
                OnPropertyChanged(nameof(LabelIsVisible));
            }
        }

        private string _labelMessage = Constants.NoDataFound;
        public string LabelMessage
        {
            get { return _labelMessage; }
            set { SetProperty(ref _labelMessage, value); }
        }

        [Obsolete]
        public HomeViewModel(INavigation nav)
        {
            navigation = nav;
            Username = "Hello " + Global.LoggedInUser.user_display_name.ToUpper() + ",";

            MessagingCenter.Subscribe<App, string>(App.Current, "UserNameUpdate", (snd, arg) =>
            {
                Username = arg;
            });
            MessagingCenter.Subscribe<App, string>(App.Current, "City", (snd, arg) =>
            {
                Address = arg;
            });
            CallHomeDataAPI();
            
        }

        public ICommand SearComm => new Command<string>((string query) =>
        {
            CallSearchAPI(SS);
        });

        public async void GoToHomeDetailPage(HomeModel homeModel)
        {
            //HomeDetailPage obj = new HomeDetailPage();
            //await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(obj);

            await navigation.PushModalAsync(new HomeDetailPage(homeModel));
        }

        [Obsolete]
        private async void CallHomeDataAPI()
        {
            await CallHomeDataAsync();
        }

        [Obsolete]
        public async Task CallHomeDataAsync()
        {
            try
            {
                PopularList = new ObservableCollection<HomeModel>();

                //DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                var response = await _homeDataAccess.CallHomeAPI();
                if (response != null)
                {
                    LabelIsVisible = false;

                    foreach (var record in response)
                    {
                        //try
                        //{
                        //    WebClient webClient = new WebClient();
                        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        //    byte[] imageBytes = webClient.DownloadData(record.fimg_url.Replace("https", "http"));
                        //    record.imageSource = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        //}
                        //catch (Exception exc)
                        //{
                        //    Console.WriteLine(exc.ToString());
                        //}

                        PopularList.Add(record);
                    }
                    OnPropertyChanged(nameof(PopularList));
                }
                else
                {
                    LabelIsVisible = true;
                }
                DependencyService.Get<ILoaderInterface>().Hide();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
            }
        }

        public async void CallSearchAPI(string searchText)
        {
            try
            {
                PopularList = new ObservableCollection<HomeModel>();

                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                ObservableCollection<HomeModel> homeModels = await _homeDataAccess.CallSearchAPI(searchText);
                DependencyService.Get<ILoaderInterface>().Hide();

                PopularList = homeModels;

            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}

