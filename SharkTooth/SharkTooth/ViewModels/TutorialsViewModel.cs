﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using SharkTooth.DataAccess;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using Xamarin.Forms;

namespace SharkTooth.ViewModels
{
    public class TutorialsViewModel : INotifyPropertyChanged
    {
        INavigation navigation;

        private ObservableCollection<SavedCardItem> savedCardItems = new ObservableCollection<SavedCardItem>();
        public ObservableCollection<SavedCardItem> TutorialDatas
        {
            get => this.savedCardItems;
            set
            {
                this.savedCardItems = value;
                OnPropertyChanged(nameof(TutorialDatas));
            }
        }


        public TutorialsViewModel(INavigation nav)
        {
            navigation = nav;
            GetTutorialPages();
            //TutorialDatas.Add(new SavedCardItem("Scan", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitationullamco labor is nisi ut aliquip exea commodo consequat Duis aute irure dolor.", ImageSource.FromFile("TutScanImage")));
            //TutorialDatas.Add(new SavedCardItem("See Information", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitationullamco labor is nisi ut aliquip exea commodo consequat Duis aute irure dolor.", ImageSource.FromFile("TutInfoImage")));
            //TutorialDatas.Add(new SavedCardItem("Search for Food outlets", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitationullamco labor is nisi ut aliquip exea commodo consequat Duis aute irure dolor.", ImageSource.FromFile("TutOutletImage")));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetProperty<T>(ref T storage, T value,
                                     [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }



        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        }

        public async void GetTutorialPages()
        {
            //DependencyService.Get<ILoaderInterface>().Show();
            ObservableCollection<Root> allPagesAPIModel = await TutorialsDataAccess.CallAllPages();
           // DependencyService.Get<ILoaderInterface>().Hide();

            if (allPagesAPIModel != null)
            {
               for(int i = 0; i < allPagesAPIModel.Count; i++)
                {
                    if(allPagesAPIModel[i].id == 44)
                    {
                        WebClient webClient = new WebClient();
                        byte[] imageBytes = webClient.DownloadData(allPagesAPIModel[i]._embedded.WpFeaturedmedia[0].source_url);
                        ImageSource source = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        TutorialDatas.Add(new SavedCardItem(allPagesAPIModel[i].title.rendered, allPagesAPIModel[i].content.rendered, source));
                    }
                    if (allPagesAPIModel[i].id == 47)
                    {
                        WebClient webClient = new WebClient();
                        byte[] imageBytes = webClient.DownloadData(allPagesAPIModel[i]._embedded.WpFeaturedmedia[0].source_url);
                        ImageSource source = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        TutorialDatas.Add(new SavedCardItem(allPagesAPIModel[i].title.rendered, allPagesAPIModel[i].content.rendered, source));
                    }

                    if (allPagesAPIModel[i].id == 49)
                    {
                        WebClient webClient = new WebClient();
                        byte[] imageBytes = webClient.DownloadData(allPagesAPIModel[i]._embedded.WpFeaturedmedia[0].source_url);
                        ImageSource source = ImageSource.FromStream(() => new MemoryStream(imageBytes));
                        TutorialDatas.Add(new SavedCardItem(allPagesAPIModel[i].title.rendered, allPagesAPIModel[i].content.rendered, source));
                    }

                }
            }
        }

    }
}
