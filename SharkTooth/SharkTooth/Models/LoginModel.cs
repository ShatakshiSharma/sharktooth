﻿using System;
namespace SharkTooth.Models
{
    public class LoginModel
    {
        public string token { get; set; }
        public string user_email { get; set; }
        public string user_nicename { get; set; }
        public string user_display_name { get; set; }
        public string user_password { get; set; }
        public string id { get; set; }
        public string phone_number { get; set; }
    }
}
