﻿using System;
using Xamarin.Forms;

namespace SharkTooth.Models
{
    public class SavedCardItem
    {
        
        public string Heading { get; set; }
        public string description { get; set; }
        public ImageSource tutImages { get; set; }

        public SavedCardItem(string heading, string description, ImageSource image)
        {
            this.Heading = heading;
            this.description = description;
            tutImages = image;
        }

        
    }
}
