﻿using System;
using System.Collections.ObjectModel;

namespace SharkTooth.Models
{
    public class UserModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public string slug { get; set; }
        public AvatarUrls avatar_urls { get; set; }
        public ObservableCollection<object> meta { get; set; }
        public string phone_number { get; set; }
        public Links _links { get; set; }
    }
}
