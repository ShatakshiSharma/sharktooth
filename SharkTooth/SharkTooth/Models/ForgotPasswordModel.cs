﻿using System;
namespace SharkTooth.Models
{
    public class ForgotPasswordModel
    {
        public string status { get; set; }
        public string msg { get; set; }
    }
}
