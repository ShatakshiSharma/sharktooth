﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SharkTooth.Models
{
    public class RegisterNonce
    {
        public string status { get; set; }
        public string controller { get; set; }
        public string method { get; set; }
        public string nonce { get; set; }
    }

    public class SignUpModel 
    {
        public int id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public string locale { get; set; }
        public string nickname { get; set; }
        public string slug { get; set; }
        public List<string> roles { get; set; }
        public DateTime registered_date { get; set; }
        public Capabilities capabilities { get; set; }
        public ExtraCapabilities extra_capabilities { get; set; }
        public AvatarUrls avatar_urls { get; set; }
        public List<object> meta { get; set; }
        public string phone_number { get; set; }
       // public Links _links { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Capabilities
    {
        public bool upload_files { get; set; }
        public bool edit_posts { get; set; }
        public bool edit_published_posts { get; set; }
        public bool publish_posts { get; set; }
        public bool read { get; set; }
        public bool level_2 { get; set; }
        public bool level_1 { get; set; }
        public bool level_0 { get; set; }
        public bool delete_posts { get; set; }
        public bool delete_published_posts { get; set; }
        public bool author { get; set; }
    }

    public class ExtraCapabilities
    {
        public bool author { get; set; }
    }

    public class AvatarUrls
    {
        public string _24 { get; set; }
        public string _48 { get; set; }
        public string _96 { get; set; }
    }

    //public class Self
    //{
    //    public string href { get; set; }
    //}

    //public class Collection
    //{
    //    public string href { get; set; }
    //}

    //public class Links
    //{
    //    public List<Self> self { get; set; }
    //    public List<Collection> collection { get; set; }
    //}
   

}

