﻿using System;
using System.Collections.ObjectModel;

namespace SharkTooth.Models
{
    public class ChangePasswordModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public string locale { get; set; }
        public string nickname { get; set; }
        public string slug { get; set; }
        public ObservableCollection<string> roles { get; set; }
        public DateTime registered_date { get; set; }
        public Capabilities capabilities { get; set; }
        public ExtraCapabilities extra_capabilities { get; set; }
        public AvatarUrls avatar_urls { get; set; }
        public ObservableCollection<object> meta { get; set; }
        public string phone_number { get; set; }
        public Links _links { get; set; }
    }

    

    

    

    
}
