﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace SharkTooth.Models
{
    public class AllPagesAPIModel
    {
        public ObservableCollection<Root> root { get;set; }
    }

    public class Root
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public DateTime date_gmt { get; set; }
        public Guid guid { get; set; }
        public DateTime modified { get; set; }
        public DateTime modified_gmt { get; set; }
        public string slug { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public string link { get; set; }
        public Title title { get; set; }
        public Content content { get; set; }
        public Excerpt excerpt { get; set; }
        public int author { get; set; }
        public int featured_media { get; set; }
        public int parent { get; set; }
        public int menu_order { get; set; }
        public string comment_status { get; set; }
        public string ping_status { get; set; }
        public string template { get; set; }
        public ObservableCollection<object> meta { get; set; }
        public bool acf_fields { get; set; }
        public Links _links { get; set; }
        public Embedded _embedded { get; set; }
    }

    public class Guid
    {
        public string rendered { get; set; }
    }

    public class Title
    {
        public string rendered { get; set; }
    }

    public class Content
    {
        public string rendered { get; set; }
        public bool @protected { get; set; }
    }

    public class Excerpt
    {
        public string rendered { get; set; }
        public bool @protected { get; set; }
    }

    public class Self
    {
        public string href { get; set; }
    }

    public class Collection
    {
        public string href { get; set; }
    }

    public class About
    {
        public string href { get; set; }
    }

    public class Author
    {
        public bool embeddable { get; set; }
        public string href { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public string slug { get; set; }
        public AvatarUrls avatar_urls { get; set; }
        public string phone_number { get; set; }
        public Links _links { get; set; }
    }

    public class Reply
    {
        public bool embeddable { get; set; }
        public string href { get; set; }
    }

    public class VersionHistory
    {
        public int count { get; set; }
        public string href { get; set; }
    }

    public class PredecessorVersion
    {
        public int id { get; set; }
        public string href { get; set; }
    }

    public class WpFeaturedmedia
    {
        public bool embeddable { get; set; }
        public string href { get; set; }
        public int id { get; set; }
        public DateTime date { get; set; }
        public string slug { get; set; }
        public string type { get; set; }
        public string link { get; set; }
        public Title title { get; set; }
        public int author { get; set; }
        public Caption caption { get; set; }
        public string alt_text { get; set; }
        public string media_type { get; set; }
        public string mime_type { get; set; }
        public MediaDetails media_details { get; set; }
        public string source_url { get; set; }
        public Links _links { get; set; }
    }

    public class WpAttachment
    {
        public string href { get; set; }
    }

    public class Cury
    {
        public string name { get; set; }
        public string href { get; set; }
        public bool templated { get; set; }
    }

    public class Links
    {
        public List<Self> self { get; set; }
        public List<Collection> collection { get; set; }
        public List<About> about { get; set; }
        public List<Author> author { get; set; }
        public List<Reply> replies { get; set; }

        [JsonProperty("version-history")]
        public List<VersionHistory> VersionHistory { get; set; }

        [JsonProperty("predecessor-version")]
        public List<PredecessorVersion> PredecessorVersion { get; set; }

        [JsonProperty("wp:featuredmedia")]
        public List<WpFeaturedmedia> WpFeaturedmedia { get; set; }

        [JsonProperty("wp:attachment")]
        public List<WpAttachment> WpAttachment { get; set; }

        public List<Cury> curies { get; set; }

        [JsonProperty("wp:term")]
        public List<WpTerm> WpTerm { get; set; }
    }

    //public class AvatarUrls
    //{
    //    public string _24 { get; set; }
    //    public string _48 { get; set; }
    //    public string _96 { get; set; }
    //}

    public class Caption
    {
        public string rendered { get; set; }
    }

    public class Sizes
    {
    }

    public class ImageMeta
    {
        public string aperture { get; set; }
        public string credit { get; set; }
        public string camera { get; set; }
        public string caption { get; set; }
        public string created_timestamp { get; set; }
        public string copyright { get; set; }
        public string focal_length { get; set; }
        public string iso { get; set; }
        public string shutter_speed { get; set; }
        public string title { get; set; }
        public string orientation { get; set; }
        public List<object> keywords { get; set; }
    }

    public class MediaDetails
    {
        public int width { get; set; }
        public int height { get; set; }
        public string file { get; set; }
        public Sizes sizes { get; set; }
        public ImageMeta image_meta { get; set; }
    }

    public class Embedded
    {
        public List<Author> author { get; set; }

        [JsonProperty("wp:featuredmedia")]
        public List<WpFeaturedmedia> WpFeaturedmedia { get; set; }
    }
}
