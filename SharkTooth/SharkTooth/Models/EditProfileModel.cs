﻿using System;
using Newtonsoft.Json;

namespace SharkTooth.Models
{
    public class EditProfileModel
    {
        public string status { get; set; }
        public Insecure insecure { get; set; }

        [JsonProperty("first name")]
        public FirstName FirstName { get; set; }

        [JsonProperty("phone number")]
        public PhoneNumber PhoneNumber { get; set; }
    }

    public class Insecure
    {
        public bool updated { get; set; }
    }

    public class FirstName
    {
        public bool updated { get; set; }
    }

    public class PhoneNumber
    {
        public bool updated { get; set; }
    }
}
