﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using Xamarin.Forms;

namespace SharkTooth.DataAccess
{
    public class TutorialsDataAccess
    {
        public TutorialsDataAccess()
        {
        }

        public static async Task<ObservableCollection<Root>> CallAllPages()
        {
            try
            {
                string jsonStr = await DataAccess.GetRequestAsync(Constants.AllPagesAPI);
                if (jsonStr != "")
                {
                   var json  = JsonConvert.DeserializeObject<ObservableCollection<Root>>(jsonStr);
                   return json;
                }
                return null;
            }
            catch(Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        } 
    }
}
