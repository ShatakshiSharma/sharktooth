﻿
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using SharkTooth.Interfaces;
using SharkTooth.SupportingFiles;
using Newtonsoft.Json;
using RestSharp;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SharkTooth.DataAccess
{
    public class DataAccess
    {
        private static RestClient _restClient = new RestClient(Constants.APIBaseUrl);

        public DataAccess()
        {
        }

        
        public static async Task<string> PostRequestAsync(string url, Dictionary<string, object> paramsDict = null, Dictionary<string, string> headersDict = null, string arrayData = null)
        {
            string responceStr = "";
            try
            {

                var current = Connectivity.NetworkAccess;

                if (current != NetworkAccess.Internet)
                {
                    DependencyService.Get<ILoaderInterface>().Hide();
                    await Application.Current.MainPage.DisplayAlert(Constants.Error, "Please check your internet connection", Constants.OK);
                    return "";
                }

                var request = new RestRequest(url, Method.POST);

                if (paramsDict != null)
                {
                    //foreach (var pair in paramsDict)
                    //{
                    //    request.AddParameter(pair.Key, pair.Value);
                    //}
                    request.AddJsonBody(paramsDict);
                }

                if (arrayData != null)
                {
                    request.AddParameter("application/json", arrayData, ParameterType.RequestBody);
                }

                if (headersDict != null)
                {
                    foreach (KeyValuePair<string, string> pair in headersDict)
                    {
                        request.AddHeader(pair.Key, pair.Value);
                    }
                }

                request.AddHeader("Content-Type", "application/json");

                var restResponse = await _restClient.ExecuteAsync(request);

                responceStr = restResponse.Content;

                return responceStr;
            }
            catch (Exception exc)
            {
                return exc.Message;
            }
        }


        public static async Task<string> GetRequestAsync(string url, Dictionary<string, object> paramsDict = null, Dictionary<string, string> headersDict = null)
        {
            string responceStr = "";
            try
            {
                var current = Connectivity.NetworkAccess;

                if (current != NetworkAccess.Internet)
                {
                    DependencyService.Get<ILoaderInterface>().Hide();
                    await Application.Current.MainPage.DisplayAlert(Constants.Error, "Please check your internet connection", Constants.OK);
                    return "";
                }

                var request = new RestRequest(url, Method.GET);

                if (paramsDict != null)
                {
                    foreach (var pair in paramsDict)
                    {
                        request.AddParameter(pair.Key, pair.Value);
                    }
                }

                if (headersDict != null)
                {
                    foreach (KeyValuePair<string, string> pair in headersDict)
                    {
                        request.AddHeader(pair.Key, pair.Value);
                    }
                }

                request.AddHeader("Content-Type", "application/json");

                var restResponse = await _restClient.ExecuteAsync(request);

                responceStr = restResponse.Content;

                return responceStr;
            }
            catch (Exception exc)
            {
                return exc.Message;
            }
        }

        public static async Task<string> PutRequestAsync(string url, Dictionary<string, object> paramsDict = null, Dictionary<string, string> headersDict = null, string arrayData = null)
        {
            string responceStr = "";
            try
            {

                var current = Connectivity.NetworkAccess;

                if (current != NetworkAccess.Internet)
                {
                    DependencyService.Get<ILoaderInterface>().Hide();
                    await Application.Current.MainPage.DisplayAlert(Constants.Error, "Please check your internet connection", Constants.OK);
                    return "";
                }

                var request = new RestRequest(url, Method.PUT);

                if (paramsDict != null)
                {
                    //foreach (var pair in paramsDict)
                    //{
                    //    request.AddParameter(pair.Key, pair.Value);
                    //}
                    request.AddJsonBody(paramsDict);
                }

                if (arrayData != null)
                {
                    request.AddParameter("application/json", arrayData, ParameterType.RequestBody);
                }

                if (headersDict != null)
                {
                    foreach (KeyValuePair<string, string> pair in headersDict)
                    {
                        request.AddHeader(pair.Key, pair.Value);
                    }
                }

                request.AddHeader("Content-Type", "application/json");

                var restResponse = await _restClient.ExecuteAsync(request);

                responceStr = restResponse.Content;

                return responceStr;
            }
            catch (Exception exc)
            {
                return exc.Message;
            }
        }


        public static async Task<string> CallMultipartDataAPI(string url, Dictionary<string, object> paramsDict = null, Dictionary<string, object> paramsDictOFFilesData = null)
        {
            string responceStr = "";
            try
            {
                var current = Connectivity.NetworkAccess;

                if (current != NetworkAccess.Internet)
                {
                    DependencyService.Get<ILoaderInterface>().Hide();
                    await Application.Current.MainPage.DisplayAlert(Constants.Error, "Please check your internet connection", Constants.OK);
                    return "";
                }

                var request = new RestRequest(url, Method.POST);

                if (paramsDict != null)
                {
                    foreach (var pair in paramsDict)
                    {
                        request.AddParameter(pair.Key, pair.Value);
                    }
                }

                if (paramsDictOFFilesData != null)
                {
                    foreach (var pair in paramsDictOFFilesData)
                    {
                        if (!String.IsNullOrEmpty(pair.Value.ToString()))
                        {
                            request.AddFile(pair.Key, pair.Value.ToString());
                        }
                    }
                }

                //request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Content-Type", "multipart/form-data");
                request.AlwaysMultipartFormData = true;

                var restResponse = await _restClient.ExecuteAsync(request);

                responceStr = restResponse.Content;

                return responceStr;
            }
            catch (Exception exc)
            {
                return exc.Message;
            }
        }

        //public static void testGameAPI() {
        //    var client = new RestClient("https://minesweeper1.p.rapidapi.com/boards/new?r=1&c=1&bombs=1");
        //    var request = new RestRequest(Method.GET);
        //    request.AddHeader("x-rapidapi-key", "2b61f60b51mshbb33b20b2d590d4p130e2fjsn6a8fd7c28a07");
        //    request.AddHeader("x-rapidapi-host", "minesweeper1.p.rapidapi.com");
        //    IRestResponse response = client.Execute(request);
        //    Console.WriteLine(response);
        //}

    }
}
