﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using Xamarin.Forms;

namespace SharkTooth.DataAccess
{
    public class SignUpDataAccess 
    {
        public SignUpDataAccess()
        {
           
        }

       
        public async Task<bool> CallPostSignUpAPI(string username, string password, string email, string phone, string name, string nonce)
        {
            bool isSuccessfull = false;

            try
            {
                string signUpURL = Constants.SignUpAPI + nonce + "&insecure=cool";
                Dictionary<string, object> dictDict = new Dictionary<string, object>();
                dictDict.Add("username", username);
                dictDict.Add("user_pass", password);
                dictDict.Add("email", email);
                dictDict.Add("phone_number", phone);
                dictDict.Add("first_name", name);

                string jsonStr = await DataAccess.GetRequestAsync(signUpURL, dictDict);
                JObject resultObj = JObject.Parse(jsonStr);

                if (jsonStr == "")
                {
                    return isSuccessfull;
                }
                if (resultObj.SelectToken("code") != null || resultObj.SelectToken("status").ToString() == "error")
                {
                    return isSuccessfull;
                }

                isSuccessfull = true;
                return isSuccessfull;
            }
            catch (Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return isSuccessfull;
            }
        }

        public async Task<RegisterNonce> CallGetNonce()
        {
            try
            {
                string jsonStr = await DataAccess.PostRequestAsync(Constants.NonceAPI, null);
                if (jsonStr != "")
                {
                    var json = JsonConvert.DeserializeObject<RegisterNonce>(jsonStr);
                    return json;
                }
                return null;
            }
            catch (Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }

        public static async Task<ForgotPasswordModel> CallForgotPasswordAPI(string email)
        {
            try
            {
                Dictionary<string, object> dictDict = new Dictionary<string, object>();
                dictDict.Add("user_login", email);
                string jsonStr = await DataAccess.GetRequestAsync(Constants.ForgotPasswordAPI, dictDict);
                if (jsonStr != "")
                {
                    var json = JsonConvert.DeserializeObject<ForgotPasswordModel>(jsonStr);
                    return json;
                }
                return null;
            }
            catch(Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }
    }
}

