﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using Xamarin.Forms;

namespace SharkTooth.DataAccess
{
    public class ProfileDataAccess
    {
        public ProfileDataAccess()
        {
        }

        public static async Task<CookieModel> CallCookieAPI(string username, string password)
        {
            try
            {
                Dictionary<string, object> dictDict = new Dictionary<string, object>();
                dictDict.Add("username", username);
                dictDict.Add("password", password);

                string jsonStr = await DataAccess.GetRequestAsync(Constants.EditProfileGetCookie, dictDict);
                if(jsonStr != "")
                {
                    var json = JsonConvert.DeserializeObject<CookieModel>(jsonStr);
                    return json;
                }
                return null;
            }
            catch(Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }

        public static async Task<EditProfileModel> SaveProfileDataAPi(string cookie, string name, string phoneNumber)
        {
            try
            {
                string jsonStr = await DataAccess.GetRequestAsync(Constants.SaveProfileDetails + "&cookie=" + cookie + "&insecure=cool&first_name=" + name + "&phone_number=" + phoneNumber);
                if(jsonStr != "")
                {
                    var json = JsonConvert.DeserializeObject<EditProfileModel>(jsonStr);
                    return json;
                }
                return null;
            }
            catch(Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }

        public static async Task<ChangePasswordModel> CallChangePasswordAPI(string newPassword)
        {
            try
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();
                dict.Add("password", newPassword);

                Dictionary<string, string> headerDict = new Dictionary<string, string>();
                headerDict.Add("Authorization", "Bearer " + Global.LoggedInUser.token);

                string jsonStr = await DataAccess.PutRequestAsync(Constants.ChangePasswordAPI  + Global.LoggedInUser.id+ "?password=" + newPassword, null, headerDict);
                if (jsonStr != null)
                {
                    var json = JsonConvert.DeserializeObject<ChangePasswordModel>(jsonStr);
                    Global.LoggedInUser.user_password = newPassword;
                    Global.LoggedInUser = Global.LoggedInUser;
                    return json;
                }
                return null;
            }
            catch (Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }

        public static async Task<FeedbackModel> CallFeedbackAPI(string feedback)
        {
            try
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();
                dict.Add("content", feedback);

                Dictionary<string, string> headerDict = new Dictionary<string, string>();
                headerDict.Add("Authorization", "Bearer " + Global.LoggedInUser.token);

                string jsonStr = await DataAccess.PostRequestAsync(Constants.FeedbackAPI, dict, headerDict);
                if (jsonStr != null)
                {
                    var json = JsonConvert.DeserializeObject<FeedbackModel>(jsonStr);
                    return json;
                }
                return null;
            }
            catch(Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
