﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace SharkTooth.DataAccess
{
    public class HomeDataAccess
    {
        public HomeDataAccess()
        {
            
        }

        public async Task<ObservableCollection<HomeModel>> CallHomeAPI()
        {
            try
            {
                string jsonStr = await DataAccess.GetRequestAsync(Constants.HomeAPI, null);
                if (jsonStr != "")
                {
                    var json = JsonConvert.DeserializeObject<ObservableCollection<HomeModel>>(jsonStr);
                    return json;
                }
                return null;
            }
            catch (Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }

        public async Task<ObservableCollection<HomeModel>> CallSearchAPI(string textResult)
        {
            try
            {
                string jsonStr = await DataAccess.GetRequestAsync(Constants.SearchAPI + "=" + textResult, null);
                if (jsonStr != "")
                {
                    var json = JsonConvert.DeserializeObject<ObservableCollection<HomeModel>>(jsonStr);
                    return json;
                }
                return null;
            }
            catch(Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }

        }

        public async Task<HomeModel> CallSharkDetailsAPI(string id)
        {
            try
            {
                string jsonStr = await DataAccess.GetRequestAsync(Constants.ShardDetailAPI + "/" + id , null);
                if(jsonStr != null)
                {
                    var json = JsonConvert.DeserializeObject<HomeModel>(jsonStr);
                    return json;
                }
                return null;
            }
            catch(Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }
    }
}

