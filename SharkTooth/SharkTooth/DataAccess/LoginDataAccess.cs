﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SharkTooth.Interfaces;
using SharkTooth.Models;
using SharkTooth.SupportingFiles;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SharkTooth.DataAccess
{
    public class LoginDataAccess
    {
        public LoginDataAccess()
        {

        }

        public async Task<bool> CallPostLoginAPI(string username, string password)
        {
            bool isLoginSuccessfull = false;

            try
            {
                Dictionary<string, object> dictDict = new Dictionary<string, object>();
                dictDict.Add("username", username);
                dictDict.Add("password", password);

                string jsonStr = await DataAccess.PostRequestAsync(Constants.LoginEndpoint, dictDict);

                if (jsonStr != "")
                {
                    Global.LoggedInUser = JsonConvert.DeserializeObject<LoginModel>(jsonStr);
                    if(Global.LoggedInUser.user_email != null)
                    {
                        Global.LoggedInUser.user_password = password;
                        Global.LoggedInUser = Global.LoggedInUser;
                        Helpers.SettingsHelper.SavedToken = JsonConvert.DeserializeObject<LoginModel>(jsonStr).token;
                        isLoginSuccessfull = true;
                        return isLoginSuccessfull;
                    }
                }
                return isLoginSuccessfull;
                
            }
            catch (Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return isLoginSuccessfull;
            }
        }

        public async Task<string> CallLoggedInUserIdAPI()
        {
            try
            {
                Dictionary<string, string> dictHeader = new Dictionary<string, string>();
                dictHeader.Add("Authorization", "Bearer " + Global.LoggedInUser.token);
                string jsonStr = await DataAccess.PostRequestAsync(Constants.GetLoggedInUserIdAPI, null, dictHeader);
                
                if (jsonStr != "")
                { 

                }
                return jsonStr;
            }
            catch(Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }

        public async Task<UserModel> GetUserData()
        {
            try
            {
                Dictionary<string, string> dictHeader = new Dictionary<string, string>();
                dictHeader.Add("Authorization", "Bearer " + Global.LoggedInUser.token);
                string jsonStr = await DataAccess.GetRequestAsync(Constants.UsersAPI + "/" + Global.LoggedInUser.id, null, dictHeader);
                if (jsonStr != "")
                {
                    var json = JsonConvert.DeserializeObject<UserModel>(jsonStr);
                    return json;
                }
                return null;
            }
            catch(Exception e)
            {
                DependencyService.Get<ILoaderInterface>().Hide();
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
