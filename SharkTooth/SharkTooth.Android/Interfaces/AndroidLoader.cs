﻿using System;
using AndroidHUD;
using SharkTooth.Interfaces;
using Xamarin.Forms;

namespace SharkTooth.Droid.Interfaces
{
    public class AndroidLoader : ILoaderInterface
    {
        public void Hide()
        {
            AndHUD.Shared.Dismiss();
        }

        public void Show(string title = "")
        {
            MainActivity myActivity = (MainActivity)Forms.Context;
            AndHUD.Shared.Show(myActivity, title, -1, MaskType.Black);
        }
    }
}

