﻿using System;
using System.ComponentModel;
using Android.Graphics.Drawables;
using Android.Widget;
using SharkTooth.CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomSearchBar), typeof(SharkTooth.Droid.CustomRenderer.CustomSearchBar))]

namespace SharkTooth.Droid.CustomRenderer
{
    public class CustomSearchBar : SearchBarRenderer
    {
        [Obsolete]
        public CustomSearchBar()
        {
        }

        [Obsolete]
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Control != null)
            {
                var color = global::Xamarin.Forms.Color.LightGray;
                var searchView = Control as SearchView;

                GradientDrawable gd = new GradientDrawable();
                gd.SetColor(Android.Graphics.Color.ParseColor("#1F8E8E93"));
                gd.SetStroke(2, global::Android.Graphics.Color.Transparent);
                gd.SetCornerRadius(12);
                this.Control.SetBackgroundDrawable(gd);

                int searchPlateId = searchView.Context.Resources.GetIdentifier("android:id/search_plate", null, null);
                Android.Views.View searchPlateView = searchView.FindViewById(searchPlateId);
                searchPlateView.SetBackgroundColor(Android.Graphics.Color.Transparent);

            }
        }
    }
}

