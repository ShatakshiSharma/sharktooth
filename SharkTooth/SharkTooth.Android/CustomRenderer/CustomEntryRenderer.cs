﻿using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Text;
using AndroidX.Core.Content;
using CustomRenderer.Android;
using SharkTooth.CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace CustomRenderer.Android
{
    public class CustomEntryRenderer : EntryRenderer
    {
        CustomEntry customEntry;
        private readonly Context _context;

        public CustomEntryRenderer(Context context) : base(context)
        {
            _context = context;

        }

        [System.Obsolete]
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null && e.NewElement != null)
            {
                customEntry = e.NewElement as CustomEntry;

                this.Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                Control.SetPadding(customEntry.MyPadding + 20, 25, 15, 0);

                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    Control.BackgroundTintList = ColorStateList.ValueOf(global::Android.Graphics.Color.Transparent);
                else
                    Control.Background.SetColorFilter(global::Android.Graphics.Color.Transparent, PorterDuff.Mode.SrcAtop);

                if (!string.IsNullOrEmpty(customEntry.Image))
                {
                    switch (customEntry.ImageAlignment)
                    {
                        case ImageAlignment.Left:
                            this.Control.SetCompoundDrawablesWithIntrinsicBounds(GetDrawable(customEntry.Image), null, null, null);
                            break;
                        case ImageAlignment.Right:
                            this.Control.SetCompoundDrawablesWithIntrinsicBounds(null, null, GetDrawable(customEntry.Image), null);
                            break;
                    }
                }

                Control.SetHintTextColor(ColorStateList.ValueOf(global::Android.Graphics.Color.Black));
            }
        }
        private BitmapDrawable GetDrawable(string imageEntryImage)
        {
            int resID = Resources.GetIdentifier(imageEntryImage, "drawable", this.Context.PackageName);
            var drawable = ContextCompat.GetDrawable(this.Context, resID);
            var bitmap = ((BitmapDrawable)drawable).Bitmap;

            return new BitmapDrawable(Resources, Bitmap.CreateScaledBitmap(bitmap, customEntry.ImageWidth * 2, customEntry.ImageHeight * 2, true));
        }
    }
}
