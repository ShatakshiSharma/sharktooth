﻿using System;
using BigTed;
using SharkTooth.Interfaces;
using Xamarin.Forms;

namespace SharkTooth.iOS.Interfaces
{
    public class iOSLoader : ILoaderInterface
    {
        public void Hide()
        {
            BTProgressHUD.Dismiss();
        }

        public void Show(string title = "")
        {
            BTProgressHUD.Show(title, maskType: ProgressHUD.MaskType.Black);
        }
    }
}

