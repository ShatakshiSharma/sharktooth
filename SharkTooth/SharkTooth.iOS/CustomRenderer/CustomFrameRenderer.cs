﻿using System;
using CoreGraphics;
using SharkTooth.CustomRenderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomFrame), typeof(CustomFrameRenderer))]
namespace SharkTooth.CustomRenderer
{
    class CustomFrameRenderer : FrameRenderer
    {

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            base.LayoutSubviews();

            var frame = (CustomFrame)Element;
            if (frame == null)
                return;



            if (frame.HasShadow)
            {
                this.Layer.ShadowColor = frame.ShadowColor.ToCGColor();
            }

            if (frame.BorderWidth > 0)
            {
                this.Layer.BorderColor = frame.BorderColor.ToCGColor();
                this.Layer.BorderWidth = frame.BorderWidth;
            }
            this.Layer.CornerRadius = Element.CornerRadius;
            this.Layer.ShadowRadius = 5.0f;
            this.Layer.ShadowColor = UIColor.LightGray.CGColor;
            this.Layer.ShadowOffset = new CGSize(0, 0);
            this.Layer.ShadowOpacity = 0.2f;
            this.Layer.ShadowPath = UIBezierPath.FromRect(Layer.Bounds).CGPath;
            this.Layer.MasksToBounds = false;
        }
    }
}

