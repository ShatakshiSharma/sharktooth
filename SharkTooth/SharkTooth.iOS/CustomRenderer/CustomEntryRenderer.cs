﻿using System;
using System.Drawing;
using CoreGraphics;
using SharkTooth.CustomRenderer;
using SharkTooth.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace SharkTooth.iOS
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null && Control != null)
            {
                var customEntry = e.NewElement as CustomEntry;

                Control.BorderStyle = UITextBorderStyle.None;
                Control.Layer.BorderColor = UIColor.LightGray.CGColor;
                Control.Layer.BorderWidth = 0;
                Control.Layer.CornerRadius = 5;
                Control.TextColor = UIColor.Black;
                Control.LeftView = new UIView(new CGRect(0, 0, customEntry.MyPadding, 0));
                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.RightView = new UIView(new CGRect(0, 0, customEntry.MyPadding, 0));
                Control.RightViewMode = UITextFieldViewMode.Always;

                if (!string.IsNullOrEmpty(customEntry.Image))
                {
                    switch (customEntry.ImageAlignment)
                    {
                        case ImageAlignment.Left:
                            Control.LeftViewMode = UITextFieldViewMode.Always;
                            Control.LeftView = GetImageView(customEntry.Image, customEntry.ImageHeight, customEntry.ImageWidth);
                            break;
                        case ImageAlignment.Right:
                            Control.RightViewMode = UITextFieldViewMode.Always;
                            Control.RightView = GetImageView(customEntry.Image, customEntry.ImageHeight, customEntry.ImageWidth);
                            break;
                    }
                }
            }
        }


        private UIView GetImageView(string imagePath, int height, int width)
        {
            var uiImageView = new UIImageView(UIImage.FromBundle(imagePath))
            {
                Frame = new RectangleF(0, 0, width, height)
            };
            UIView objLeftView = new UIView(new System.Drawing.Rectangle(0, 0, width + 10, height));
            objLeftView.AddSubview(uiImageView);

            return objLeftView;
        }
    }
}
